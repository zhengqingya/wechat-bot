# 🤖微信机器人🤖

> UOS可用 => 见https://github.com/wechaty/puppet-wechat/pull/129

### 参考

- https://github.com/yaphone/itchat4j
- https://github.com/wxmbaci/itchat4j-uos

---

### 简单demo

见 [AppTest.java](src/test/java/com/zhengqing/demo/AppTest.java)

```java
public class AppTest {
    public static void main(String[] args) {
        // 实现IMsgHandlerFace接口的类
        IMsgHandlerFace msgHandler = new CustomMsgHandler();
        // 保存登陆二维码图片的路径
        Wechat wechat = new Wechat(msgHandler, new File("./login").getAbsolutePath());
        // 启动服务，会在login下生成一张二维码图片，扫描即可登陆，注意，二维码图片如果超过一定时间未扫描会过期，过期时会自动更新，所以你可能需要重新打开图片
        wechat.start();
    }
}
```

实现[IMsgHandlerFace](src/main/java/cn/zhouyafeng/itchat4j/face/IMsgHandlerFace.java)类定义自己的消息处理逻辑...

> ex: [CustomMsgHandler.java](src/test/java/com/zhengqing/demo/CustomMsgHandler.java)

```java
public interface IMsgHandlerFace {
    /**
     * 处理文本消息
     */
    String textMsgHandle(BaseMsg msg);

    /**
     * 处理图片消息
     */
    String picMsgHandle(BaseMsg msg);

    /**
     * 处理声音消息
     */
    String voiceMsgHandle(BaseMsg msg);

    /**
     * 处理小视频消息
     */
    String viedoMsgHandle(BaseMsg msg);

    /**
     * 处理名片消息
     */
    String nameCardMsgHandle(BaseMsg msg);

    /**
     * 处理系统消息
     */
    void sysMsgHandle(BaseMsg msg);

    /**
     * 处理确认添加好友消息
     */
    String verifyAddFriendMsgHandle(BaseMsg msg);

    /**
     * 处理收到的文件消息
     */
    String mediaMsgHandle(BaseMsg msg);
}
```

#### 工具类：

- 消息处理类 [MessageTools](src/main/java/cn/zhouyafeng/itchat4j/api/MessageTools.java)
- 小工具类(ex: 获取好友列表) [WechatTools](src/main/java/cn/zhouyafeng/itchat4j/api/WechatTools.java)

## TIPS

> 不要频繁切换登录，否则会有提示违规！
> 这个功能感受下即可，别多玩...
> ![img.png](images/wechat-01.png)
> ![img.png](images/wechat-02.png)

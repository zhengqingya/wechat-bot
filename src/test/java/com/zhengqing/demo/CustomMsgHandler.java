package com.zhengqing.demo;

import cn.zhouyafeng.itchat4j.api.MessageTools;
import cn.zhouyafeng.itchat4j.api.WechatTools;
import cn.zhouyafeng.itchat4j.beans.BaseMsg;
import cn.zhouyafeng.itchat4j.beans.RecommendInfo;
import cn.zhouyafeng.itchat4j.core.Core;
import cn.zhouyafeng.itchat4j.face.IMsgHandlerFace;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * <p> 消息处理 </p>
 *
 * @author zhengqingya
 * @description
 * @date 2022/12/13 15:26
 */
@Slf4j
public class CustomMsgHandler implements IMsgHandlerFace {

    @Override
    public String textMsgHandle(BaseMsg msg) {
        if (msg.isGroupMsg()) {
            // 群消息不处理
            return null;
        }
        String text = msg.getText();
        if (text.equals("666")) {
            // 查看能拿到的数据...
            Core core = WechatTools.getCore();
            List<JSONObject> contactList = WechatTools.getContactList();
        }
        return "🤖自动回复：消息已收到";
    }

    @Override
    public String picMsgHandle(BaseMsg msg) {
        if (msg.isGroupMsg()) {
            // 群消息不处理
            return null;
        }
        return "🤖自动回复：图片已收到";
    }

    @Override
    public String voiceMsgHandle(BaseMsg msg) {
        if (msg.isGroupMsg()) {
            // 群消息不处理
            return null;
        }
        return "🤖自动回复：声音已收到";
    }

    @Override
    public String viedoMsgHandle(BaseMsg msg) {
        if (msg.isGroupMsg()) {
            // 群消息不处理
            return null;
        }
        return "🤖自动回复：视频已收到";
    }

    @Override
    public String nameCardMsgHandle(BaseMsg msg) {
        if (msg.isGroupMsg()) {
            // 群消息不处理
            return null;
        }
        return "🤖自动回复：名片已收到";
    }

    @Override
    public void sysMsgHandle(BaseMsg msg) {
        String text = msg.getContent();
        log.info("收到系统消息: {}", text);
    }

    @Override
    public String verifyAddFriendMsgHandle(BaseMsg msg) {
        // 同意好友请求，false为不接受好友请求
        MessageTools.addFriend(msg, true);
        RecommendInfo recommendInfo = msg.getRecommendInfo();
        String nickName = recommendInfo.getNickName();
        String province = recommendInfo.getProvince();
        String city = recommendInfo.getCity();
        String text = "你好，来自" + province + city + "的" + nickName + "， 欢迎添加我为好友！";
        return text;
    }

    @Override
    public String mediaMsgHandle(BaseMsg msg) {
        if (msg.isGroupMsg()) {
            // 群消息不处理
            return null;
        }
        return null;
    }

}

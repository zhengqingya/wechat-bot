package com.zhengqing.demo;

import cn.zhouyafeng.itchat4j.Wechat;
import cn.zhouyafeng.itchat4j.face.IMsgHandlerFace;

import java.io.File;

/**
 * <p> 测试 </p>
 *
 * @author zhengqingya
 * @description
 * @date 2022/12/13 15:23
 */
public class AppTest {
    public static void main(String[] args) {
        // 实现IMsgHandlerFace接口的类
        IMsgHandlerFace msgHandler = new CustomMsgHandler();
        // 保存登陆二维码图片的路径
        Wechat wechat = new Wechat(msgHandler, new File("./login").getAbsolutePath());
        // 启动服务，会在login下生成一张二维码图片，扫描即可登陆，注意，二维码图片如果超过一定时间未扫描会过期，过期时会自动更新，所以你可能需要重新打开图片
        wechat.start();
    }
}
